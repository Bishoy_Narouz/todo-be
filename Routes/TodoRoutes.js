let TodoService = require('../Domain/TodoService');
let express = require('express')
    , router = express.Router();
let errorCode = require('../Domain/Enums/ErrorCodeEnum');
let validateToken = require('../Services/validateToken');

router.get('/GetUserTodoList', async function (req, res) {
    try {
        let result = await TodoService.GetUserTodoList(req.query.userId);
        res.send({ Success: true, Message: '', Data: result });

    } catch (e) {
        console.log(e);
        res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
    }
})

router.post('/createTodo', async function (req, res) {
    try {
        let todo = {
            userId: req.body.userId,
            text: req.body.text,
            createdAt: req.body.createdAt,
            deleted: req.body.deleted,
            done: req.body.done
        }
        let result = await TodoService.createTodo(todo);
        res.send({ Success: true, Message: 'Todo Created Successfully', Data: result });

    } catch (e) {
        // console.log(e);
        res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
    }
})

router.delete('/deleteItem', async function (req, res) {
    try {
        let result = await TodoService.deleteItem(req.query._id);
        res.send({ Success: true, Message: 'Item Deleted Successfully' });
    } catch (e) {
        console.log(e);
        res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
    }
})

router.put('/doneItem', async function (req, res) {
    try {
        let result = await TodoService.doneItem(req.body._id);
        res.send({ Success: true, Message: 'Item Updated Successfully' });
    } catch (e) {
        console.log(e);
        res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
    }
})

module.exports = router;