let AccountService = require('../Domain/AccountService');
let express = require('express')
    , router = express.Router()
let errorCode = require('../Domain/Enums/ErrorCodeEnum');
const jwt = require('jsonwebtoken');
const config = require('../config');
const { check, validationResult } = require('express-validator/check')

router.post('/Login', [check('identity').exists(), check('password').exists()], async function (req, res) {
    try {
        let user = {
            identity: req.body.identity,
            password: req.body.password
        }
        let result = await AccountService.Login(user);
        let token = jwt.sign({ Id: result._id }, config.Secret, {
            expiresIn: '1h'
        });
        result.token = token;
        res.send({
            Success: true,
            Message: '',
            Data: result
        });

    } catch (e) {
        console.log(e);
        if (e.message === errorCode.INVALID_CREDENTIALS) {
            res.send({ Success: false, Message: errorCode.INVALID_CREDENTIALS });
        }
        else if (e.message === errorCode.ACCOUNT_IS_NOT_VERIFIED) {
            res.send({ Success: false, Message: errorCode.ACCOUNT_IS_NOT_VERIFIED });
        }
        else {
            res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
        }
    }
})

router.post('/Register', [check('username').exists(), check('email').exists().isEmail(), check('password').exists()], async function (req, res) {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() });
        }
        let user = {
            email: req.body.email,
            username: req.body.username,
            password: req.body.password
        }
        let result = await AccountService.Register(user);
        res.send({ Success: true, Message: 'you have registered successfully' });

    } catch (e) {
        if (e.message === errorCode.USER_HAS_REGISTERED_BEFORE) {
            res.send({ Success: false, Message: errorCode.USER_HAS_REGISTERED_BEFORE });
        } else {
            res.send({ Success: false, Message: errorCode.UNKNOWN_SERVER_ERROR });
        }
    }
})

module.exports = router