module.exports = {
    BackendUrl: 'localhost',
    BackendPort: 5000,
    FrontendUrl: 'http://localhost:4200/',
    DBUrl: 'mongodb://localhost:27017/todo-app',
    corsOptions: {
        origin: '*',
        optionsSuccessStatus: 200
    },
    Secret: 'efwdTWcTo6yDqpJlu0iRfN6UEtPoxy3bxSA+hQQdf965NVDh/1lfWrRcBAyhrGYEEjHkp4Pku1NNXO4qS4KN1A=='
}