const mongoose = require('mongoose');
const config = require('../config');

module.exports = (function () {
    mongoose.connect(config.DBUrl, { useNewUrlParser: true });
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', function () {
        // console.log('we are connected');
    });
})();

