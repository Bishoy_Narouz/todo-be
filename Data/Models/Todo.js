let mongoose = require('mongoose');

let todo = new mongoose.Schema({
    text: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        require: true
    },
    done: {
        type: Boolean,
        require: true
    }
});

let Todo = mongoose.model('Todo', todo);
module.exports = Todo; 