let mongoose = require('mongoose');

let user = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: false
    }
});

// Compile model from schema
let User = mongoose.model('User', user);
module.exports = User; 