let AccountRepository = module.exports = {}
let User = require('../Models/User');
const bcrypt = require('bcrypt');

AccountRepository.findUserByEmailOrUsername = async function (user) {
    let result = await User.findOne({
        $or: [
            { email: user.email },
            { username: user.username },
        ]
    });
    return result;
};

AccountRepository.findUserByIdentity = async function (identity) {
    let result = await User.findOne({
        $or: [
            { email: identity },
            { username: identity },
        ]
    });
    return result;
};

AccountRepository.createUser = async function (user) {
    let result = await User.create(
        {
            email: user.email,
            username: user.username,
            password: bcrypt.hashSync(user.password, 15) 
        });
    return result;
};
