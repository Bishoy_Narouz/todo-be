let TodoRepository = module.exports = {}
let Todo = require('../Models/Todo');

TodoRepository.findTodoByUserId = async function (userId) {
    let result = await Todo.find({ userId: userId }).sort({ createdAt: 1 });
    return result;
};

TodoRepository.createTodo = async function (todo) {
    let result = await Todo.create(
        {
            userId: todo.userId,
            text: todo.text,
            createdAt: todo.createdAt,
            deleted: todo.deleted,
            done: todo.done
        }
    );
    return result;
};

TodoRepository.deleteItem = async function (_id) {
    let result = await Todo.deleteOne({ _id: _id });
    return result;
};

TodoRepository.doneItem = async function (_id) {
    let result = await Todo.updateOne({ _id: _id }, { $set: { done: true } });
    return result;
};