const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors')
const AccountRoutes = require('./Routes/AccountRoutes');
const TodoRoutes = require('./Routes/TodoRoutes');
const config = require('./config');
const db = require('./Data/DBConfig');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors(config.corsOptions));
app.use('/api/Account', AccountRoutes);
app.use('/api/Todo', TodoRoutes);

app.listen(config.BackendPort, () => console.log(`the app listening on port ${config.BackendPort}`));
