const AccountService = module.exports = {}
const AccountRepository = require('../Data/Repositories/AccountRepository');
const randomstring = require('randomstring');
const errorCode = require('../Domain/Enums/ErrorCodeEnum');
const bcrypt = require('bcrypt');

AccountService.Login = async function (model) {
    let user = await AccountRepository.findUserByIdentity(model.identity);
    if (!user || !bcrypt.compareSync(model.password, user.password)) {
        throw new Error(errorCode.INVALID_CREDENTIALS);
    } else {
        return { _id: user._id, username: user.username, email: user.email };
    }
};

AccountService.Register = async function (model) {
    let user = await AccountRepository.findUserByEmailOrUsername(model);
    if (user) {
        throw new Error(errorCode.USER_HAS_REGISTERED_BEFORE);
    } else {
        await AccountRepository.createUser(model);
        return;
    }
};





