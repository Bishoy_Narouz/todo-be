const TodoService = module.exports = {}
const TodoRepository = require('../Data/Repositories/TodoRepository');

TodoService.GetUserTodoList = async function (userId) {
    let todoList = await TodoRepository.findTodoByUserId(userId);
    return todoList;
}

TodoService.createTodo = async function (todo) {
    let result = await TodoRepository.createTodo(todo);
    return result;
}

TodoService.deleteItem = async function (_id) {
    let result = await TodoRepository.deleteItem(_id);
    return result;
}

TodoService.doneItem = async function (_id) {
    let result = await TodoRepository.doneItem(_id);
    return result;
}